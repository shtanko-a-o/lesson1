package lessonOne;


public class Test {
    public static int sum(int...numbers) {
        int result = 0;
        for (int i = 0; i < numbers.length; i++) {
            result += numbers[i];
        }
        return result;
    }

    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        try {
            a = Integer.parseInt(args[0]);
            b = Integer.parseInt(args[1]);
            System.out.println(sum(a, b));
        } catch (NumberFormatException e) {
            System.out.println("Error! Entered not a number!");
        }

    }
}
